let burger = document.getElementById("burger");
let burgerMenu = document.getElementById("burger-menu");

burger.addEventListener('click', toggleBurgerMenu);

function toggleBurgerMenu() {
    burgerMenu.classList.toggle('is-open');
    burger.classList.toggle('is-open');
}

  